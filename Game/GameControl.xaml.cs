﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Runtime.Remoting.Channels;
using System.Data;
using System.Windows.Threading;
using System.Timers;

namespace Game
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class GameControl : UserControl
    {
        DispatcherTimer t;
        DateTime start = DateTime.Now;
        
        Singleton player = Singleton.CreateInstance();
        private string polygon_id_to_transceive = string.Empty;
        private int build_transmitter_vertex_id = 0;
        private int vertex_id = 0;
        public int PolygonNumber = 0;

        

        private void t_Tick(object sender, EventArgs e)
        {
            //DateTime myDate = DateTime.ParseExact((DateTime.Now - start).ToString(), "HH:mm:ss",
            //System.Globalization.CultureInfo.InvariantCulture);
            DateTime duration_time = DateTime.Parse((DateTime.Now - start).ToString());
            TimerDisplay.Content = duration_time.ToString("mm:ss");
        }


        public GameControl(string welcomeText)
        {
            t = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 50), DispatcherPriority.Background,
                t_Tick, Dispatcher.CurrentDispatcher); t.IsEnabled = true;
            start = DateTime.Now;

            InitializeComponent();
            txtBlckGeneralInfo.Text = txtBlckGeneralInfo.Text + string.Format("\n{0}", welcomeText);
            UpdateUserInfo();
            
            /*Uruchomienie nasłuchiwania na info od serwera*/
            Thread tcpServerRunThread = new Thread(new ThreadStart(TcpServerRun));
            tcpServerRunThread.Start();

            /*Timer do aktualizacji danych co 5 sekund*/
            System.Timers.Timer timerUpdateData = new System.Timers.Timer();
            timerUpdateData.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);
            timerUpdateData.Interval = 5000;
            timerUpdateData.Enabled = true;
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            UpdateMapFromAnotherThread();
            UpdateUserInfoFromAnotherThread();
        }

        public string GetTextBlockGeneralInfoText()
        {
            return string.Format("{0}", txtBlckGeneralInfo.Text);
        }

        public void SetTextBlockGeneralInfoText(string text)
        {
            txtBlckGeneralInfo.Text = text;
        }

        void NoticeHexagonAdded1(object sender, NpgsqlNotificationEventArgs e)
        {
            throw new NotImplementedException();
        }
        
        void btn2_fun2_Click(object sender, RoutedEventArgs e)
        {
        }


        void myConnection_Notice(object sender, NpgsqlNoticeEventArgs e)
        {
            MessageBox.Show("Dodano nowy obszar!");
        }

        
        private void Children_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            polygon_id_to_transceive = "";
            var polygon = sender as Polygon;
            var length = polygon.Name.Length;
            polygon_id_to_transceive = polygon.Name.Substring(1, length - 1);
            txtBlckGeneralInfo.Text = string.Format("{0}\n{1}{2}", "Informacje", "Wybrałeś obszar numer: ", polygon_id_to_transceive);

        }
        //private void GvMap_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    var gv = sender as Grid;
        //    var x = gv.Children;
        //    MessageBox.Show("Wybrałeś obszar numer: ");
        //    var y = gv.Children[0];
        //    int i = 1;
        //}


        #region Updating map data - updating buttons background - showning opponents transceivers
        private void UpdateMap()
        {
            DataTable dt = Tools.UpdateButtonsBackgrounds(player.game_id);
            if (dt.Rows.Count>0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ImageBrush brush = new ImageBrush(new BitmapImage(new Uri("img/transmitter_" + row.Field<string>("db_color") + ".png", UriKind.Relative)));
                    ((Button)(FindName(string.Format("btn{0}", row.Field<int>("db_vertex_id"))))).Background = brush;
                }
            }
        }
        private void UpdateMapFromAnotherThread()
        {
            DataTable dt = Tools.UpdateButtonsBackgrounds(player.game_id);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ImageBrush brush = new ImageBrush(new BitmapImage(new Uri("img/transmitter_" + row.Field<string>("db_color") + ".png", UriKind.Relative)));
                    this.Dispatcher.Invoke(new Action(() => ((Button)(FindName(string.Format("btn{0}", row.Field<int>("db_vertex_id"))))).Background = new ImageBrush(new BitmapImage(new Uri("img/transmitter_" + row.Field<string>("db_color") + ".png", UriKind.Relative)))));
                }
            }
        }
        #endregion

        #region Thred running while building transceiver, enables correct Polygon id selection
        private void RunThread()
        {
            while (true)
            {
                if (!string.IsNullOrEmpty(polygon_id_to_transceive) && build_transmitter_vertex_id != 0)
                {
                    BuildTransmitterResponse response = new BuildTransmitterResponse();
                    response.BuildTransmitter(build_transmitter_vertex_id, Int32.Parse(polygon_id_to_transceive), player.id);
                    Action<int> updateAction = new Action<int>((value) => txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", response.Message));
                    txtBlckGeneralInfo.Dispatcher.Invoke(updateAction, 32);

                    polygon_id_to_transceive = string.Empty;

                    if (response.CodeType == CodeType.success)
                    {
                        this.Dispatcher.Invoke(new Action(() => ((Button)(FindName(string.Format("btn{0}", build_transmitter_vertex_id)))).Background = new ImageBrush(new BitmapImage(new Uri("img/transmitter_" + player.color + ".png", UriKind.Relative)))));
                        //this.Dispatcher.Invoke(new Action(() => (btn17).Background = new ImageBrush(new BitmapImage(new Uri("img/transmitter_" + "blue" + ".png", UriKind.Relative)))));
                    }
                    this.Dispatcher.Invoke(new Action(() => Tools.EnableAllPolygons(Polygons.Children)));
                    return;
                }
                else
                    Thread.Sleep(100);
            }
        }
        #endregion

        #region Updating User Info
        private void UpdateUserInfo()
        {
            List<string> user_info = Tools.GetData(player.id);
            lblPlayerName.Content = user_info.ElementAt(0);
            lblPlayerCash.Content = user_info.ElementAt(1);
            lblPlayerVertexCount.Content = user_info.ElementAt(2);
            lblPlayerAbonentsCount.Content = user_info.ElementAt(3);
            lblPlayerRoundSalary.Content = user_info.ElementAt(4);
            cbxCanPlay.IsChecked = Boolean.Parse(user_info.ElementAt(5));
        }

        private void UpdateUserInfoFromAnotherThread()
        {
            List<string> user_info = Tools.GetData(player.id);
            Action<int> updateAction0 = new Action<int>((value) => lblPlayerName.Content = user_info.ElementAt(0));
            lblPlayerName.Dispatcher.Invoke(updateAction0, 32);

            Action<int> updateAction1 = new Action<int>((value) => lblPlayerCash.Content = user_info.ElementAt(1));
            lblPlayerCash.Dispatcher.Invoke(updateAction1, 32);

            Action<int> updateAction2 = new Action<int>((value) => lblPlayerVertexCount.Content = user_info.ElementAt(2));
            lblPlayerVertexCount.Dispatcher.Invoke(updateAction2, 32);

            Action<int> updateAction3 = new Action<int>((value) => lblPlayerAbonentsCount.Content = user_info.ElementAt(3));
            lblPlayerAbonentsCount.Dispatcher.Invoke(updateAction3, 32);

            Action<int> updateAction4 = new Action<int>((value) => lblPlayerRoundSalary.Content = user_info.ElementAt(4));
            lblPlayerRoundSalary.Dispatcher.Invoke(updateAction4, 32);

            Action<int> updateAction5 = new Action<int>((value) => cbxCanPlay.IsChecked = Boolean.Parse(user_info.ElementAt(5)));
            cbxCanPlay.Dispatcher.Invoke(updateAction5, 32);
        }

        private void btnSyncUserInfo_Click(object sender, RoutedEventArgs e)
        {
            UpdateUserInfo();
        } 
        #endregion

        #region Server
        private void TcpServerRun()
        {
            //TcpListener tcpListener = new TcpListener(IPAddress.Parse(Tools.GetServerIPAddress()), 5000);
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 5000);
            tcpListener.Start();

            while (true)
            {
                TcpClient client = tcpListener.AcceptTcpClient();
                Thread tcpHandlerThread = new Thread(new ParameterizedThreadStart(TcpHandler));
                tcpHandlerThread.Start(client);
            }

        }

         public void TcpHandler(object client)
        {
            TcpClient mClient = (TcpClient)client;
            NetworkStream stream = mClient.GetStream();
            byte[] buffer = new byte[mClient.ReceiveBufferSize];

            int readBytes = stream.Read(buffer, 0, mClient.ReceiveBufferSize);

            string data = Encoding.UTF8.GetString(buffer, 0, readBytes);
            Message m = Message.DeserializeToObject(data);
            if (m.Type == 1)
            {
                player.color = m.Color;
                player.game_id = m.PlayerId;
            }
            else if (m.Type == 2)
            {
                Action<int> updateRoundLabelAction = new Action<int>((value) => lblRound.Content = string.Format("{0}", m.PlayerId));
                lblRound.Dispatcher.Invoke(updateRoundLabelAction, 32);
            }
            else if (m.Type == 3)
            {
                UpdateMapFromAnotherThread();
            }
            //f(m);
            Action<int> updateAction = new Action<int>((value) => txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", m.Text));
            txtBlckGeneralInfo.Dispatcher.Invoke(updateAction, 32);

            stream.Close();
            mClient.Close();
        }
        public void f(Message m)
        {

            player.color = m.Color;
            player.id = m.PlayerId;
            MessageBox.Show(m.Color);

        }
        #endregion

        #region Polygons - Mouse Events - Polygon Info

        private void SetPolygonPopupInfo(int polygon_id)
        {
            Tuple<string, int, int, string> info = Tools.GetPolygonInfo(polygon_id);

            lblPolygon00.Content = "Nazwa obszaru:";
            lblPolygon01.Content = "Populacja:";
            lblPolygon02.Content = "Pozostało osób:";
            lblPolygon03.Content = "Właściciele:";

            txbPolygon10.Text = info.Item1;
            txbPolygon11.Text = info.Item2.ToString();
            txbPolygon12.Text = info.Item3.ToString();
            txbPolygon13.Text = info.Item4;
        }

        private void P1_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P1_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P2_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P2_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P3_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }


        private void P3_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P4_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P4_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P5_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P5_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P6_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P6_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P7_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P7_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P8_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P8_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P9_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P9_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P10_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P10_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P11_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P11_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P12_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P12_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P13_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P13_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P14_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P14_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P15_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P15_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P16_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P16_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        private void P17_MouseEnter(object sender, MouseEventArgs e)
        {
            var Polygon = sender as Polygon;
            var name = Polygon.Name;
            int polygon_id = Int32.Parse(name.Substring(1, name.Length - 1));
            SetPolygonPopupInfo(polygon_id);
            OponentImagePopUp.IsOpen = true;
        }

        private void P17_MouseLeave(object sender, MouseEventArgs e)
        {
            OponentImagePopUp.IsOpen = false;
        }

        #endregion

        #region Buttons - click actions - disabling Polygons for clicking and buildind Transceivers
        private void StartTransceiverBuilding()
        {
            polygon_id_to_transceive = string.Empty;
            MessageBox.Show("Wybierz obszar, który obejmie swoim zasięgiem nadajnik!", "Informacja", MessageBoxButton.OK);
            foreach (Polygon children in Polygons.Children)
            {
                children.MouseLeftButtonDown += Children_MouseLeftButtonDown;
            }
            Thread lThread = new Thread(RunThread);
            lThread.Start();
        }

        private void btn1_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn2_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1", "P2" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn3_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P2" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn4_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P2" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn5_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P2", "P3" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn6_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P3", "P4" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn7_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P4" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn8_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P4" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn9_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P4", "P5" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn10_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P5" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn11_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn12_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1", "P2", "P7" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn13_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P2", "P3", "P7" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn14_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P3", "P4", "P9" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn15_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P4", "P5", "P9" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn16_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P5" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn17_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1", "P6" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn18_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P1", "P6", "P7" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn19_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P3", "P7", "P8" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn20_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P3", "P8", "P9" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn21_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P5", "P9", "P10" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn22_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P5", "P10" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn23_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P6" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn24_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P6", "P7", "P12" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn25_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P7", "P8", "P12" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn26_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P8", "P9", "P14" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn27_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P9", "P10", "P14" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn28_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P10" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn29_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P6", "P11" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn30_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P6", "P11", "P12" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn31_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P8", "P12", "P13" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn32_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P8", "P13", "P14" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn33_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P10", "P14", "P15" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn34_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P10", "P15" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn35_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P11" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn36_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P11", "P12", "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn37_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P12", "P13", "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn38_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P13", "P14", "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn39_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P14", "P15", "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn40_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P15" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn41_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P11" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn42_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P11", "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn43_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P13", "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn44_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P13", "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn45_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P15", "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn46_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P15" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");      
        }

        private void btn47_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn48_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P16" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn49_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
            }

        private void btn50_buildTransmitter(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                string btn_name = (sender as MenuItem).Name;
                build_transmitter_vertex_id = Int32.Parse(btn_name.Substring(1));
                List<string> l = new List<string>(new string[] { "P17" });
                Tools.DisableSpecificPolygons(Polygons.Children, l);
                StartTransceiverBuilding();
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        #endregion

        #region - Buttons - click actions - upgrading Indicators
        private void btn1QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(1, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
      
        private void btn1MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(1, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn2QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(2, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn2MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(2, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn3QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(3, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn3MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(3, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn4QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(4, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn4MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(4, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn5QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(5, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn5MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(5, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn6QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(6, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn6MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(6, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn7QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(7, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn7MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(7, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn8QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(8, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn8MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(8, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn9QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(9, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn9MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(9, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn10QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(10, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn10MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(10, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn11QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(11, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn11MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(11, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn12QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(12, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn12MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(12, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn13QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(13, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn13MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(13, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn14QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(14, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn14MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(14, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn15QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(15, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn15MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(15, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn16QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(16, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn16MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(16, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn17QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(17, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn17MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(17, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn18QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(18, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn18MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(18, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn19QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(19, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn19MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(19, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn20QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(20, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn20MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(20, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn21QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(21, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn21MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(21, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
         }

        private void btn22QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(22, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn22MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(22, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn23QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(23, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn23MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(23, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn24QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(24, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn24MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(24, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn25QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(25, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn25MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(25, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn26QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(26, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn26MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(26, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn27QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(27, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn27MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(27, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn28QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(28, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn28MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(28, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn29QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(29, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn29MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(29, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn30QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(30, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
         }

        private void btn30MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(30, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn31QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(31, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn31MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(31, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn32QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(32, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn32MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(32, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn33QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(33, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn33MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(33, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn34QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(34, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn34MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(34, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn35QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(35, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn35MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(35, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn36QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(36, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn36MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(36, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn37QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(37, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn37MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(37, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn38QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(38, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn38MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(38, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn39QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(39, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn39MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(39, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn40QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(40, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn40MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(40, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn41QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(41, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn41MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(41, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn42QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(42, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn42MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(42, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn43QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(43, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn43MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(43, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn44QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(44, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn44MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(44, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn45QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(45, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn45MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(45, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn46QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(46, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn46MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(46, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn47QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(47, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn47MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(47, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn48QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(48, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn48MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(48, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn49QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(49, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn49MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(49, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        private void btn50QInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(4, player.id, 1);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }

        private void btn50MInd_Click(object sender, RoutedEventArgs e)
        {
            if (Tools.CheckTurnPermission(player.id))
            {
                Tuple<int, string> upgrade_response = Tools.UpgradeIndicator(4, player.id, 2);
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", upgrade_response.Item2);
            }
            else
                txtBlckGeneralInfo.Text = string.Format("{0}\n{1}", "Informacje", "Zaczekaj na swój ruch!");
        }
        #endregion

        #region Buttons - MouseEnter, MouseLeave Events - Vertexes and Indicators Info
        private void SetVertexPopupInfo(int vertex_id)
        {
            List<string> list = Tools.GetVertexData(vertex_id);

            lblVertex00.Content = "Numer wierzchołka:";
            lblVertex01.Content = "Gracz:";
            lblVertex02.Content = "Wskaźnik Jakości:";
            lblVertex03.Content = "Wskaźnik Marketingu:";
            lblVertex04.Content = "Koszt ulepszenia Jakości:";
            lblVertex05.Content = "Koszt ulepszenia Marketingu:";

            txbVertex10.Text = list.ElementAt(0);
            txbVertex11.Text = list.ElementAt(1);
            txbVertex12.Text = list.ElementAt(2);
            txbVertex13.Text = list.ElementAt(3);
            txbVertex14.Text = list.ElementAt(4);
            txbVertex15.Text = list.ElementAt(5);
        }

        private void btn1_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn1_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn2_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn2_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn3_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn3_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn4_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn4_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn5_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn5_MouseLeave(object sender, MouseButtonEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn6_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn6_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn7_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn7_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn8_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn8_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn9_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn9_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn10_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn10_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn11_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn11_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn12_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn12_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn13_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn13_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn14_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn14_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn15_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn15_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn16_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn16_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn17_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn17_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn18_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn18_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn19_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;

        }

        private void btn19_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn20_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn20_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn21_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn21_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn22_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn22_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn23_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn23_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn24_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn24_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn25_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn25_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn26_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn26_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn27_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn27_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn28_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;

        }

        private void btn28_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;

        }

        private void btn29_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn29_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn30_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn30_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn31_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn31_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn32_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn32_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn33_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }
        

        private void btn33_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn34_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn34_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn35_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn35_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;

        }

        private void btn36_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn36_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn37_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn37_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn38_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn38_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn39_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn39_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn40_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn40_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn41_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn41_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn42_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn42_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;

        }

        private void btn43_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;

        }

        private void btn43_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn44_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn44_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn45_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn45_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn46_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn46_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn47_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn47_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn48_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;

        }

        private void btn48_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }

        private void btn49_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;
        }

        private void btn49_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;

        }

        private void btn50_MouseEnter(object sender, MouseEventArgs e)
        {
            string btn_name = (sender as Button).Name;
            vertex_id = Int32.Parse(btn_name.Substring(3));
            SetVertexPopupInfo(vertex_id);
            VertexInfoPopup.IsOpen = true;

        }

        private void btn50_MouseLeave(object sender, MouseEventArgs e)
        {
            VertexInfoPopup.IsOpen = false;
        }
        #endregion
    }
}

