﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Game
{
    public class TxbLoginValidation : ValidationRule
    {
        public override ValidationResult Validate (object value, System.Globalization.CultureInfo cultureinfo)
        {
            string login = string.Empty;
            try
            {
                login = Convert.ToString(value);
                if (string.IsNullOrEmpty(login))
                    return new ValidationResult(false, "Value must not be empty");
                else
                    return new ValidationResult(true, null);
            }
            catch (Exception)
            {
                return new ValidationResult(false, "Value must not be empty");
            }
        }
    }
}
