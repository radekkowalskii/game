﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace Game
{
    public class Tools
    {
        public static string GetServerIPAddress()
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", 1);
                    cmd.CommandText = "SELECT text FROM config WHERE id = @id";
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }

        public static bool CheckTurnPermission(int player_id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = "SELECT canplay FROM users WHERE id = @id";
                    return Boolean.Parse(cmd.ExecuteScalar().ToString());
                }
            }
        }

        public static string GetPlayerColor(int player_id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = "SELECT color FROM users WHERE id = @id";
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }
        public static int GetPlayerCash(int player_id)
        {
            int cash = 0;
            string ret_str = string.Empty;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = "SELECT cash FROM users WHERE id = @id";
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr.HasRows && dr["cash"] != DBNull.Value && dr["cash"] != null)
                            {
                                Int32.TryParse(dr["cash"].ToString(), out cash);
                                return cash;

                            }
                            else
                                return cash;
                        }
                     }
                    return cash;
                }
            }
        }

        public static List<string> GetData(int player_id)
        {
            List<string> list = new List<string>();
            string username = string.Empty;
            string cash = string.Empty;
            string client_number = string.Empty;
            string vertex_number = string.Empty;
            string round_salary = string.Empty;
            string canplay = string.Empty;
            int price_per_client = GetPricePerClient();

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", player_id);
                    cmd.CommandText = @"SELECT u.username, u.cash, u.clients_number, u.canplay ,(SELECT COUNT(*) FROM vertex WHERE player_id = @id) AS vertex_number FROM users u WHERE id = @id";
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            username = dr["username"].ToString();
                            cash = dr["cash"].ToString();
                            vertex_number = dr["vertex_number"].ToString();
                            client_number = dr["clients_number"].ToString();
                            canplay = dr["canplay"].ToString();
                        }
                    }
                }
            }
            if (client_number != "0" && !string.IsNullOrEmpty(client_number))
            {
                round_salary = string.Format("{0}", (Int32.Parse(client_number) * price_per_client)); 
            }
            if (string.IsNullOrEmpty(canplay))
            {
                canplay = false.ToString();
            }

            list.Add(username);
            list.Add(cash);
            list.Add(vertex_number);
            list.Add(client_number);
            list.Add(round_salary);
            list.Add(canplay);

            return list;
        }

        public static List<string> GetVertexData(int vertex_id)
        {
            List<string> list = new List<string>();
            string username = string.Empty;
            string id= String.Format("{0}", vertex_id);
            string quality_level = string.Empty;
            string marketing_level = string.Empty;
            string q_ind_upgrade_cost = string.Empty;
            string m_ind_upgrade_cost = string.Empty;

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", vertex_id);
                    cmd.Parameters.AddWithValue("indicator_type", 1);
                    cmd.CommandText = @"SELECT i.level ,u.username FROM indicator i 
                                            LEFT JOIN users u ON i.player_id = u.id 
                                        WHERE i.vertex_id = @id  AND i.type::integer = @indicator_type AND i.player_id IS NOT NULL 
                                    ORDER BY level DESC LIMIT 1;";
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                username = dr["username"].ToString();
                                quality_level = dr["level"].ToString();
                            } 
                        }
                    }
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("id", vertex_id);
                    cmd.Parameters.AddWithValue("indicator_type", 2);
                    cmd.CommandText = @"SELECT i.level ,u.username FROM indicator i 
                                            LEFT JOIN users u ON i.player_id = u.id 
                                        WHERE i.vertex_id = @id  AND i.type::integer = @indicator_type AND i.player_id IS NOT NULL 
                                    ORDER BY level DESC LIMIT 1;";
                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                username = dr["username"].ToString();
                                marketing_level = dr["level"].ToString();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(quality_level) && !string.IsNullOrEmpty(marketing_level))
                    {

                        cmd.Parameters.AddWithValue("id", vertex_id);
                        cmd.Parameters.AddWithValue("indicator_type", 1);
                        cmd.Parameters.AddWithValue("level", Int32.Parse(quality_level) + 1);
                        cmd.CommandText = @"SELECT cost FROM indicator WHERE level = @level and type::INTEGER = @indicator_type AND vertex_id = @id";
                        using (NpgsqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    q_ind_upgrade_cost = dr["cost"].ToString();
                                }
                            }
                        }
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("id", vertex_id);
                        cmd.Parameters.AddWithValue("indicator_type", 2);
                        cmd.Parameters.AddWithValue("level", Int32.Parse(marketing_level) + 1);
                        cmd.CommandText = @"SELECT cost FROM indicator WHERE level = @level and type::INTEGER = @indicator_type AND vertex_id = @id";
                        using (NpgsqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    m_ind_upgrade_cost = dr["cost"].ToString();
                                }
                            }
                        } 
                    }
                }
            }
            list.Add(id);
            list.Add(username);
            list.Add(quality_level);
            list.Add(marketing_level);
            list.Add(q_ind_upgrade_cost);
            list.Add(m_ind_upgrade_cost);
            return list;
        }

        public static Tuple<int,string> UpgradeIndicator(int vertex_id, int player_id, int indicator_type)
        {
            #region ładowanie danych z bazy do DataTable i odczyt po nazwach kolumn
            //DataTable dt = new DataTable();
            //using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            //{
            //    conn.Open();
            //    using (NpgsqlCommand cmd = conn.CreateCommand())
            //    {
            //        cmd.Parameters.AddWithValue("id", vertex_id);
            //        cmd.Parameters.AddWithValue("type",indicator_type.ToString());
            //        cmd.CommandText = "SELECT player_id, level FROM indicator WHERE vertex_id = @id AND type=@type ORDER BY level DESC LIMIT 1";
            //        using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
            //        {
            //            da.Fill(dt);
            //        }
            //        if (dt.Rows.Count == 0 || dt.Rows[0].Field<int>("player_id").ToString() == "" || dt.Rows[0].Field<int>("player_id")==player_id)
            //        {
            //            cmd.Parameters.AddWithValue("id", vertex_id);
            //            cmd.Parameters.AddWithValue("type", indicator_type);
            //            cmd.Parameters.AddWithValue("level", dt.Rows.Count == 0 ?  1 : dt.Rows[0].Field<int>("level") + 1);
            //            cmd.Parameters.AddWithValue("player_id", player_id);
            //            cmd.CommandText = "INSERT INTO indicator (type, level, player_id, vertex_id) VALUES (@type,@level,@player_id,@id);";
            //            cmd.ExecuteNonQuery();
            //        }
            //    }
            //} 
            #endregion

            
            bool permission = false;
            int cost = 0;
            int player_cash = 0;
            int curr_max_ind_level = 0;

            player_cash = Tools.GetPlayerCash(player_id);

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("vertex_id", vertex_id);
                    cmd.Parameters.AddWithValue("player_id", player_id);
                    cmd.Parameters.AddWithValue("type", indicator_type);
                    cmd.CommandText = "SELECT true AS permission, level FROM indicator WHERE vertex_id = @vertex_id AND player_id = @player_id and type::integer = @type ORDER BY level DESC LIMIT 1;";

                    using (NpgsqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                            while (dr.Read())
                        {
                            bool.TryParse(dr["permission"].ToString(), out permission);
                            //Int32.TryParse(dr["cost"].ToString(), out cost);
                            Int32.TryParse(dr["level"].ToString(), out curr_max_ind_level);
                            
                            if (curr_max_ind_level >= 4)
                            {
                                return new Tuple<int, string>(1, "Wskaźnik rozbudowany do maksimum!");
                                //jak kod zwrotki tuple.Item1 == 1, to pobrac wartosc stringa i wyswietlić w miejscu informacji
                            }
                            else if (permission == false)
                            {
                                return new Tuple<int, string>(1, "Wybrany wierzchołek nie należy do Ciebie!");
                                //jak kod zwrotki tuple.Item1 == 1, to pobrac wartosc stringa i wyswietlić w miejscu informacji
                            } 
                        }
                        else
                        {
                            return new Tuple<int, string>(1, "Wybrany wierzchołek nie należy do Ciebie!");
                            //jak kod zwrotki tuple.Item1 == 1, to pobrac wartosc stringa i wyswietlić w miejscu informacji
                        }

                    }
                    cmd.Parameters.AddWithValue("level_to_which_upgrade", curr_max_ind_level + 1);
                    cmd.Parameters.AddWithValue("vertex_id", vertex_id);
                    cmd.Parameters.AddWithValue("type", indicator_type);
                    cmd.CommandText = "SELECT cost FROM indicator WHERE vertex_id = @vertex_id AND type::INTEGER = @type AND level = @level_to_which_upgrade";

                    Int32.TryParse(cmd.ExecuteScalar().ToString(),out cost);
                    
                    if (permission == true && player_cash >= cost)
                    {
                        cmd.Parameters.AddWithValue("vertex_id", vertex_id);
                        cmd.Parameters.AddWithValue("type", indicator_type);
                        cmd.Parameters.AddWithValue("level", curr_max_ind_level + 1);
                        cmd.Parameters.AddWithValue("player_id", player_id);
                        cmd.CommandText = "UPDATE indicator SET player_id = @player_id WHERE type::integer = @type AND level = @level AND vertex_id = @vertex_id";
                        cmd.ExecuteNonQuery();
                        if (indicator_type ==1)
                        {
                            return new Tuple<int, string>(0, String.Format("{0}", "Zwiększyłeś poziom wskaźnika Jakości"));
                        }
                        else
                            return new Tuple<int, string>(0, String.Format("{0}","Zwiększyłeś poziom wskaźnika Marketingu"));

                    }
                    else if (player_cash < cost)
                    {
                        return new Tuple<int, string>(2, "Nie posiadasz wystarczających środków!");
                        //jak kod zwrotki tuple.Item1 == 2, to pobrac wartosc stringa i wyswietlić w miejscu informacji
                    }
                    else
                        return new Tuple<int, string>(3, "Nieoczekiwany błąd, spróbuj ponownie!");
                    //jak kod zwrotki tuple.Item1 == 3, to pobrac wartosc stringa i wyswietlić w miejscu informacji
                }
            }
        }

        public static void DisableSpecificPolygons(System.Windows.Controls.UIElementCollection list_of_polygons, List<string> list_not_to_disable)
        {
            List<string> not_to_disable = new List<string>();
                foreach (Polygon polygon in list_of_polygons)
                {
                    if (!list_not_to_disable.Any(lista => lista.Contains(polygon.Name)))
                    {
                        not_to_disable.Add(polygon.Name);
                        polygon.IsEnabled = false;
                    }
                }
        }

        public static void EnableAllPolygons(System.Windows.Controls.UIElementCollection list_of_polygons)
        {
            foreach(Polygon polygon in list_of_polygons)
            {
                polygon.IsEnabled = true;
            }
        }
        public static Tuple<string, int, int, string> GetPolygonInfo(int polygon_id)
        {
            
            DataTable dt = new DataTable();
            string polygon_name = string.Empty;
            int population = 0;
            int people_left = 0;
            string user_name = string.Empty;
            string user_names = string.Empty;
            

            using (NpgsqlConnection con = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                using (NpgsqlCommand cmd = con.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", polygon_id);
                    cmd.CommandText = @"SELECT h.name, h.population, h.people_left, 
                                               ARRAY_TO_STRING(ARRAY(SELECT username FROM users WHERE  users.id = ANY(h.ids_users)),',') AS names 
                                        FROM hexagon h WHERE h.id = @id";

                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                    
                    if (dt.Rows[0].Field<string>("name") != string.Empty)
                    {
                        polygon_name = dt.Rows[0].Field<string>("name");
                    }
                    if (dt.Rows[0].Field<int>("population") != null)
                    {
                        population = dt.Rows[0].Field<int>("population");
                    }
                    if (dt.Rows[0].Field<int>("people_left") != null)
                    {
                        people_left = dt.Rows[0].Field<int>("people_left");
                    }
                    if (dt.Rows[0].Field<string>("names") != string.Empty)
                    {
                        user_names = dt.Rows[0].Field<string>("names");
                    }
                }

            }

            Tuple<string, int, int, string> response = new Tuple<string, int, int, string>(polygon_name, population, people_left, user_names);
            return response;
        }

        public static void AddPlayerIdToHexahonUsersArray(int id_user_update, int hexagon_id)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                List<int> ids_players = new List<int>();
                DataTable dt = new DataTable();

                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", hexagon_id);
                    cmd.CommandText = "SELECT UNNEST(ids_users) FROM hexagon WHERE id = @id";
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            ids_players.Add(Int32.Parse(row[0].ToString()));
                        }
                    }

                    if (!ids_players.Contains(id_user_update))
                    {
                        ids_players.Add(id_user_update);
                        ids_players.Sort();

                        cmd.Parameters.AddWithValue("ids_users", ids_players.ToArray());
                        cmd.Parameters.AddWithValue("id", hexagon_id);
                        cmd.Parameters.AddWithValue("ids_users_updating", true);
                        cmd.CommandText = "UPDATE hexagon SET ids_users = @ids_users, ids_users_updating = @ids_users_updating WHERE id = @id;";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public static int GetPricePerClient()
        {
            int price = 0;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("id", 1);
                    cmd.CommandText = "SELECT value_int from setup where id = @id";
                    Int32.TryParse(cmd.ExecuteScalar().ToString(), out price);
                }

            }
            return price;
        }

        public static DataTable UpdateButtonsBackgrounds(int game_id)
        {
            DataTable dt = new DataTable();
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("game_id", game_id);
                    cmd.CommandText = @"SELECT v.id AS db_vertex_id, u.id AS player_id , u.color as db_color 
                                            FROM vertex v, users u 
                                                WHERE v.player_id = u.id AND u.game_id = @game_id;";
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }
}
