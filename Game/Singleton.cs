﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /*
     * sealed - uniemożliwia dziedziczenie po klasie Singleton
     */
    public sealed class Singleton
    {
        private static Singleton user;
        public string login { get; set; }
        public int id { get; set; }
        public string color { get; set; }
        public string IP { get; set; }
        public int game_id { get; set; }

        private Singleton() {}

        public static Singleton  CreateInstance()
        {
                if(user == null)
                {
                    user = new Singleton();
                }
                return user;
            }
    }
}
