﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public enum CodeType { success, failure, unexpectedError };

    public class BuildTransmitterResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public CodeType CodeType = CodeType.failure;


        public BuildTransmitterResponse BuildTransmitter(int vertex_id, int polygon_id, int player_id)
        {
            BuildTransmitterResponse response = new BuildTransmitterResponse();
            if (Tools.GetPlayerCash(player_id) >= 500000)
            {
                try
                {
                    using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                    {
                        int people_left = 0;

                        conn.Open();
                        conn.Notice += new NoticeEventHandler(myConnection_Notice);
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.Parameters.AddWithValue("id", vertex_id);
                            cmd.Parameters.AddWithValue("locked", true);
                            cmd.Parameters.AddWithValue("player_id", player_id);
                            cmd.CommandText = "SELECT locked FROM vertex WHERE id = @id";
                            if (Convert.ToBoolean(cmd.ExecuteScalar().ToString()) != true)
                            {
                                cmd.CommandText = "UPDATE vertex SET locked = @locked, player_id = @player_id WHERE id = @id;";
                                cmd.ExecuteNonQuery();

                                cmd.Parameters.AddWithValue("polygon_id", polygon_id);
                                cmd.CommandText = "SELECT people_left FROM hexagon WHERE id = @polygon_id";

                                Int32.TryParse(cmd.ExecuteScalar().ToString(), out people_left);

                                if (people_left >= 1000000)
                                {
                                    cmd.Parameters.AddWithValue("polygon_id", polygon_id);
                                    cmd.Parameters.AddWithValue("current_population", people_left - 1000000);
                                    cmd.Parameters.AddWithValue("id_player", player_id);
                                    cmd.Parameters.AddWithValue("ids_users_updating", false);
                                    cmd.CommandText = @"UPDATE hexagon SET people_left = @current_population, id_user_last_updated = @id_player, ids_users_updating = @ids_users_updating 
                                                        WHERE id = @polygon_id;";
                                    cmd.ExecuteNonQuery();

                                    Tools.AddPlayerIdToHexahonUsersArray(player_id, polygon_id);

                                    CodeType = CodeType.success;
                                    Message = Message + " i zyskałeś 1 000 000 abonentów!";
                                    return response;
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("polygon_id", polygon_id);
                                    cmd.Parameters.AddWithValue("current_population", 0);
                                    cmd.Parameters.AddWithValue("id_player", player_id);

                                    cmd.Parameters.AddWithValue("ids_users_updating", false);
                                    cmd.CommandText = @"UPDATE hexagon SET people_left = @current_population, id_user_last_updated = @id_player, ids_users_updating = @ids_users_updating
                                                        WHERE id = @polygon_id;";
                                    cmd.ExecuteNonQuery();

                                    Tools.AddPlayerIdToHexahonUsersArray(player_id, polygon_id);

                                    CodeType = CodeType.success;
                                    Message = Message + " i zyskałeś " + people_left + " abonentów!";

                                    return response;
                                }
                            }
                            else
                            {
                                CodeType = CodeType.failure;
                                Message = "Nie możesz zbudować nadajnika w tym miejscu!";
                                return response;
                            }
                        }
                    }
                }

                catch (NpgsqlException ex) { }
                catch (Exception ex1) { }

                CodeType = CodeType.unexpectedError;
                Message = "Wystąpił nieoczekiwany błąd! Rozpocznij rozgrywkę od nowa...";
                return response;
            }
            else
            {
                CodeType = CodeType.failure;
                Message = "Nie masz wystarczającej ilości środków do budowy nadajnika.";
                return response;
            }
        }
        private void myConnection_Notice(object sender, NpgsqlNoticeEventArgs e)
        {
            Message = e.Notice.MessageText;
            //MessageBox.Show(e.Notice.MessageText);
        }

    }
}
