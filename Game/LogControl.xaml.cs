﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Game
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class LogControl : UserControl
    {
        public string login = string.Empty;
        public string welcomeText;
        private string WelcomeText
        {
            get
            {
                 return welcomeText;
            }
            set
            {
                welcomeText = value;
            }
        }
        public LogControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            login = txbLogin.Text;
            object last_id = null;
            Validate();

            Singleton player = Singleton.CreateInstance();
            player.login = txbLogin.Text;
            player.IP = GetIPAddress();

            if (validationErrors.Count == 0)
            {
                try
                {
                    using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString))
                    {
                        conn.Open();
                        conn.Notice += new NoticeEventHandler(myConnection_Notice);
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.Parameters.AddWithValue("username", txbLogin.Text);
                            cmd.Parameters.AddWithValue("ingame", false);
                            cmd.Parameters.AddWithValue("ip", player.IP);
                            cmd.Parameters.AddWithValue("cash", 2000000);
                            cmd.Parameters.AddWithValue("canplay", false);
                            cmd.Parameters.AddWithValue("clients_number", 0);
                            cmd.CommandText = @"INSERT INTO users (username, ingame, ip, cash, canplay, clients_number) 
                                                          VALUES (@username, @ingame, @ip::inet, @cash, @canplay, @clients_number) 
                                                            RETURNING id;";
                            //cmd.ExecuteNonQuery();
                            last_id = cmd.ExecuteScalar();
                        }
                    }
                }
                catch { NpgsqlException ex; }

                player.id = Int32.Parse(last_id.ToString());

                this.Content = new GameControl(welcomeText);
            }
            else
            {
                lblError.Visibility = Visibility.Visible;
                lblError.FontSize = 22;
                lblError.Content = "Podaj login!";
                lblError.HorizontalAlignment = HorizontalAlignment.Center;
                lblError.HorizontalContentAlignment = HorizontalAlignment.Center;
                lblError.Foreground = new SolidColorBrush(Colors.Red);
            }
        }


        
        

        #region Validation
        Dictionary<string, string> validationErrors = new Dictionary<string, string>();

        void Validate()
        {
            validationErrors.Clear();
            if (string.IsNullOrWhiteSpace(login)) // Validate Surname 
            {
                validationErrors.Add("Surname", "Surname is mandatory.");
            }
            
            // Call OnPropertyChanged(null) to refresh all bindings and have WPF check the this[string columnName] indexer.
           // OnPropertyChanged(null);
        }

        #region IDataErrorInfo Members
        public string Error
        {
            get
            {
                if (validationErrors.Count > 0)
                {
                    return "Errors found.";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (validationErrors.ContainsKey(columnName))
                {
                    return validationErrors[columnName];
                }
                return null;
            }
        }

        #endregion
        #endregion
        void myConnection_Notice(object sender, NpgsqlNoticeEventArgs e)
        {
            //MessageBox.Show(e.Notice.MessageText);
            WelcomeText = e.Notice.MessageText;
        }

        private string GetIPAddress()
        {
            //IPHostEntry host;
            //string IP = string.Empty;
            //host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (IPAddress ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        IP = ip.ToString();
            //    }
            //}

            //return IP;

            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            return localIP;
        }
    }
}

