﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;

namespace Game
{
    public class Message
    {
        public int Type { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public int PlayerId { get; set; }


        public Message(int type, string text, string color, int playerId)
        {
            this.Type = type;
            this.Text = text;
            this.Color = color;
            this.PlayerId = playerId;
        }
        public Message() { }

        //public static string Serialize1(int type, string text)
        //    {
        //        Message message = new Message(type, text);
        //        string x = JsonConvert.SerializeObject(message);
        //        return string.Format("{0}", JsonConvert.SerializeObject(message));
        //    }
        public string SerializeToJson()
        {
            return string.Format("{0}", JsonConvert.SerializeObject(this));
        }
        public static Message DeserializeToObject(string text)
        {
            Message message = JsonConvert.DeserializeObject<Message>(text);
            return message;
        }

    }

}
