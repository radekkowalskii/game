﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Game
{
    public class Hexagon : Page
    {
        public static Polygon CreateAPolygon(int Ax, int Ay, int Bx, int By, int Cx, int Cy, int Dx, int Dy, int Ex, int Ey, int Fx, int Fy)
        {
            // Create a blue and a black Brush
            SolidColorBrush yellowBrush = new SolidColorBrush();
            yellowBrush.Color = Colors.Yellow;
            SolidColorBrush blackBrush = new SolidColorBrush();
            blackBrush.Color = Colors.Black;

            // Create a Polygon
            Polygon yellowPolygon = new Polygon();
            yellowPolygon.Stroke = blackBrush;
            yellowPolygon.Fill = yellowBrush;
            yellowPolygon.StrokeThickness = 2;

            // Create a collection of points for a polygon
            System.Windows.Point Point1 = new System.Windows.Point(Ax, Ay);
            System.Windows.Point Point2 = new System.Windows.Point(Bx, By);
            System.Windows.Point Point3 = new System.Windows.Point(Cx, Cy);
            System.Windows.Point Point4 = new System.Windows.Point(Dx, Dy);
            System.Windows.Point Point5 = new System.Windows.Point(Ex, Ey);
            System.Windows.Point Point6 = new System.Windows.Point(Fx, Fy);
            //System.Windows.Point Point7 = new System.Windows.Point(210, 100);
            //System.Windows.Point Point8 = new System.Windows.Point(110, 10);
            PointCollection polygonPoints = new PointCollection();
            polygonPoints.Add(Point1);
            polygonPoints.Add(Point2);
            polygonPoints.Add(Point3);
            polygonPoints.Add(Point4);
            polygonPoints.Add(Point5);
            polygonPoints.Add(Point6);
            //polygonPoints.Add(Point7);
            //polygonPoints.Add(Point8);

            // Set Polygon.Points properties
            yellowPolygon.Points = polygonPoints;
            return yellowPolygon;

            // Add Polygon to the page
            //LayoutRoot.Children.Add(yellowPolygon);
        }
    }
}
