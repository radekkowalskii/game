﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Game
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Content = new LogControl();
            //Polygon A1 = Hexagon.CreateAPolygon(20, 0, 50, 0, 70, 30, 50, 60, 20, 60, 0, 30);
            //LayoutRoot.Children.Add(A1);
            //Polygon A2 = Hexagon.CreateAPolygon(70, 30, 100, 30, 120, 60, 100, 90, 70, 90, 50, 60);
            //LayoutRoot.Children.Add(A2);
            //Polygon A3 = Hexagon.CreateAPolygon(100, 30, 120, 0, 150, 0, 170, 30, 150, 60, 120, 60);
            //LayoutRoot.Children.Add(A3);
            //Polygon B1 = Hexagon.CreateAPolygon(20, 60, 50, 60, 70, 90, 50, 120, 20, 120, 0, 90);
            //LayoutRoot.Children.Add(B1);
            //Polygon B2 = Hexagon.CreateAPolygon(70, 90, 100, 90, 120, 120, 100, 150, 70, 150, 50, 120);
            //LayoutRoot.Children.Add(B2);
            //Polygon B3 = Hexagon.CreateAPolygon(100, 90, 120, 60, 150, 60, 170, 90, 150, 120, 120, 120);
            //LayoutRoot.Children.Add(B3);
            //Polygon C1 = Hexagon.CreateAPolygon(0, 150, 20, 120, 50, 120, 70, 150, 50, 180, 20, 180);
            //LayoutRoot.Children.Add(C1);
            //Polygon C3 = Hexagon.CreateAPolygon(100, 150, 120, 120, 150, 120, 170, 150, 150, 180, 120, 180);
            //LayoutRoot.Children.Add(C3);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new LogControl();
        }

        
    }
}
